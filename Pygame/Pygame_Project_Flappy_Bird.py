#Flappy Bird!
#By: Saif Habib
global x,y,xx,yy,msg,score,color,red,blue,green,white,black,s,a,aqua,yellow,fontobj,msgobj,screen,ychange
import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((640,640))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
lightblue = (0,191,250)
black = (0,0,0)
silver = (192,192,192)
teal = (0,128,128)
brown = (165,42,42)
purple = (128,0,128)
gray = (128,128,128)
pink = (255, 0, 255)
aqua = (0,255,215)
yellow = (255, 255, 0)
y = 320
s1 = pygame.image.load('flappy.png')
s1 = pygame.transform.scale(s1,(100,100))
score = 0
x = 320
ychange = 0
a=640
s = random.randint(0,320)
pygame.display.set_caption("Flappy Bird!!!                               By: Saif Habib")
def show_text(msg,xx,yy,color):
    fontobj = pygame.font.SysFont("freesans",32)
    msgobj = fontobj.render(msg,False,color)
    screen.blit(msgobj,(xx,yy))
def collision():
    if y not in range(s-20,s+100):
        if a in range(180,440):
            for event in pygame.event.get():
                if event.type==KEYDOWN:
                    if event.key == K_h:
                        pass
                    else:
                        exit()
def game():
    collision()
    global x,y,xx,yy,msg,color,score,red,blue,green,white,black,s,a,aqua,yellow,fontobj,msgobj,screen,ychange
    for event in pygame.event.get():
        if y<=50:
            exit()
        elif y>=570:
            exit()
        elif event.type == KEYUP:
            ychange=2
        elif event.type == KEYDOWN and event.key != K_h:
            ychange=-2
    y = y+ychange
    a=a-1
    if a!=0:
        pygame.draw.rect(screen,silver,(a,0,170,640))
        pygame.draw.rect(screen,gray,(a,s,170,640),5)
        pygame.draw.rect(screen,lightblue,(a-5,s,180,180))
    elif a==0:
        a=640
        s=random.randint(0,520)
    if 320==a+180:
          score+=1
    show_text(str(score),50,50,black)
time.sleep(1)
global x,y,xx,yy,score,msg,color,red,blue,green,white,black,s,a,aqua,yellow,fontobj,msgobj,screen,ychange
while True:
    game()
    collision()
    pygame.draw.rect(screen,brown,(0,620,640,20))
    screen.blit(s1,(x,y))
    pygame.display.update()
    screen.fill(lightblue)
    pygame.draw.rect(screen,brown,(0,620,640,20))
