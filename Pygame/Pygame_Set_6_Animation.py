import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((640,640))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
black = (0,0,0)
bg = pygame.image.load('gem1.png')    #Blue Square Gem
gg = pygame.image.load('gem2.png')    #Green Circle Gem
blg = pygame.image.load('gem6.png')   #Black Square Gem
tg = pygame.image.load('gem7.png')     #Transparent Triangle Gem
Gems = [bg,gg,blg,tg]
y=0
x=0
while True:
    #if x==1:
    screen.blit(bg,(x,y))
    #if x==2:
    screen.blit(gg,(x+120,y))
    #if x==3:
    screen.blit(blg,(x+240,y))
    #if x==4:
    screen.blit(tg,(x+340,y))
    y+=1
    x+=1
    pygame.display.update()
    if y>=640:
        y=0
        screen.fill(black)
    if x>=640:
        x=0
        
