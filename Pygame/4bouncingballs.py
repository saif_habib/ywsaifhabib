import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((960,960))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
aqua = (0,255,215)
white = (255,255,255)
black = (0,0,0)
pygame.display.set_caption("Pong!!!                               By: Saif Habib")
class Ball:
    def __init__(self,x,y,color,radius):
        self.color = color
        self.radius = radius
        self.x = x
        self.y = y
    def draw(self):
        pygame.draw.circle(screen,self.color,(self.x, self.y),self.radius)
    def move(self,xchange,ychange):
        self.x = self.x + xchange
        self.y = self.y + ychange    
ball1=Ball(150,150,green,20)
xchange1 = 28
ychange1 = 21
ball2=Ball(320,320,blue,20)
xchange2 = 23
ychange2 = 29
ball3=Ball(450,450,red,20)
xchange3 = 24
ychange3 = 22
ball4=Ball(520,450,aqua,20)
xchange4 = 22
ychange4 = 28
while True:
    ball1.draw()
    ball1.move(xchange1,ychange1)
    ball2.draw()
    ball2.move(xchange2,ychange2)
    ball3.draw()
    ball3.move(xchange3,ychange3)
    ball4.draw()
    ball4.move(xchange4,ychange4)
    pygame.display.update()
    screen.fill(black)
    if ball1.x>960:
        xchange1=-xchange1
    if ball1.x<0:
        xchange1=-xchange1
    if ball1.y>960:
        ychange1=-ychange1
    if ball1.y<0:
        ychange1=-ychange1
    if ball2.x>960:
        xchange2=-xchange2
    if ball2.x<0:
        xchange2=-xchange2
    if ball2.y>960:
        ychange2=-ychange2
    if ball2.y<0:
        ychange2=-ychange2
    if ball3.x>960:
        xchange3=-xchange3
    if ball3.x<0:
        xchange3=-xchange3
    if ball3.y>960:
        ychange3=-ychange3
    if ball3.y<0:
        ychange3=-ychange3
    if ball4.x>960:
        xchange4=-xchange4
    if ball4.x<0:
        xchange4=-xchange4
    if ball4.y>960:
        ychange4=-ychange4
    if ball4.y<0:
        ychange4=-ychange4
    if ball1.x -20<= ball2.x<ball1.x+20 and ball1.y-20<= ball2.y<ball1.y+20:
        xchange1=-xchange1
        xchange2=-xchange2
        ychange1=-ychange1
        ychange2=-ychange2
    if ball2.x -20<= ball3.x<ball2.x+20 and ball2.y-20<= ball3.y<ball2.y+20:
        xchange2=-xchange2
        xchange3=-xchange3
        ychange2=-ychange2
        ychange3=-ychange3
    if ball1.x -20<= ball3.x<ball1.x+20 and ball1.y-20<= ball3.y<ball1.y+20:
        xchange1=-xchange1
        xchange3=-xchange3
        ychange1=-ychange1
        ychange3=-ychange3
    if ball1.x -20<= ball4.x<ball1.x+20 and ball1.y-20<= ball4.y<ball1.y+20:
        xchange1=-xchange1
        xchange4=-xchange4
        ychange1=-ychange1
        ychange4=-ychange4
    if ball2.x -20<= ball4.x<ball2.x+20 and ball2.y-20<= ball4.y<ball2.y+20:
        xchange2=-xchange2
        xchange4=-xchange4
        ychange2=-ychange2
        ychange4=-ychange4
    if ball3.x -20<= ball4.x<ball3.x+20 and ball3.y-20<= ball4.y<ball3.y+20:
        xchange3=-xchange3
        xchange4=-xchange4
        ychange3=-ychange3
        ychange4=-ychange4
