import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((640,480))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
black = (0,0,0)
#Colorful Circle
def ColorfulCircle():
    pygame.display.set_caption("Colorful Circles!!!                               By: Saif Habib")
    size = 300
    size_ = 300
    aa=0
    z = 0
    z=0
    aaa=1
    bb=0
    x = blue
    for l in range(0,76,1):
        pygame.display.update()
        pygame.draw.circle(screen,x,(320,240),size, size_)
        time.sleep(0.005)
        pygame.display.update()
        pygame.draw.circle(screen,black,(320,240),size, size_)
        time.sleep(0.005)
        pygame.display.update()
        if size >= 225:
            x = red
        else:
            x =green
        if size != 0 and aa ==0 or z ==1:
            size -= 10
            size_ -= 10
        elif size ==0 or bb == 1:
            aa+=1
            size+=10
            size_+=10
            aaa=0
            bb =1
        elif size == 300 and aaa == 0:
            bb=0
            a=0
            size+=10
            size_+=10
            z = 1
            aaa = 1
#Random Rectangles
def RandomRectangles():
    pygame.display.set_caption("Random Rectangles!!!                      By: Saif Habib")
    for x in range(0,250,1):
        a=random.randint(0,640)
        b=random.randint(0,480)
        d = [(255,0,0),(0,0,255),(0,255,0),(255,255,255)]
        random.shuffle(d)
        c = d[0]
        pygame.draw.rect(screen,c,(a,b,a,b),5)
        time.sleep(0.01)
        pygame.display.update()
        screen.fill(black)
#Try-A-Triangle
def TryATriangle():
    pygame.display.set_caption("Try - A - Triangle!!!                     By: Saif Habib")
    pygame.draw.polygon(screen, green, ((320,160),(213,240),(427,240)),10)
    pygame.display.update()
    time.sleep(1)
    pygame.draw.circle(screen, white, (320,200),10,10)
    pygame.display.update()
    pygame.display.set_caption("Coincidence???                           By: Saif Habib")
    time.sleep(0.5)
    screen.fill(black)
#Chess Board
def ChessBoard():
    #Each Rectangle == 80x60 pixels
    pygame.display.set_caption("Chess Board!!!                            By: Saif Habib")
    d = [(255,0,0),(0,0,255),(0,255,0)]
    random.shuffle(d)
    c = d[0]
    x=0
    y=0
    d = [(255,0,0),(0,0,255),(0,255,0)]
    for sss in range(0,8,1):
        for ss in range(0,8,1):
            random.shuffle(d)
            c = d[0]
            pygame.draw.rect(screen,c,(x,y,80,60),10)
            x+=80
            pygame.display.update()
        y+=60
        x=0
    pygame.display.update()
def ALL():
    ColorfulCircle()
    RandomRectangles()
    TryATriangle()
    ChessBoard()
ALL()
