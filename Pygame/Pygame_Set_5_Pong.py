#PONG!
#By: Saif Habib
global x1,x2,y,x,z,l,x1change,x2change,xchange,ychange,player1score,player2score
import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((1280,640))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
black = (0,0,0)
aqua = (0,255,215)
pygame.display.set_caption("Pong!!!                               By: Saif Habib")
player1score=0
player2score=0
def show_text(msg,xx,yy,color):
    fontobj = pygame.font.SysFont("freesans",32)
    msgobj = fontobj.render(msg,False,color)
    screen.blit(msgobj,(xx,yy))
def Pong():
    global x1,x2,y,x,z,l,x1change,x2change,xchange,ychange,player1score,player2score
    z=0
    l=0
    x=640
    y=random.randint(20, 620)
    x1 = 240
    x2 = 240
    x1change=0
    x2change=0
    print('Enter your level:')
    print("Easy")
    print("Medium")
    print("Hard")
    print("Impossible")
    choice = input()
    if choice == "Easy":
        xchange=2
        ychange=3
    elif choice == "Medium":
        xchange=4
        ychange=5
    elif choice == "Hard":
        xchange=7
        ychange=8
    elif choice == "Impossible":
        xchange=11
        ychange=12
    else:
        choice = "Easy"
        xchange=2
        ychange=2
    def game():
        show_text("Player 1: "+ str(player1score), 10,10,red)
        show_text("Player 2: "+ str(player2score), 1100,10,blue)
        show_text(choice,600,10,aqua)
        if player1score!=10 and player2score!=10:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_w:
                        x1change=-2
                    elif event.key == K_s:
                        x1change=2
                    elif event.key == K_UP:
                        x2change=-2
                    elif event.key == K_DOWN:
                        x2change=2
                if event.type == KEYUP:
                    if event.key == K_w:
                        x1change=0
                    elif event.key == K_s:
                        x1change=0
                    elif event.key == K_UP:
                        x2change=0
                    elif event.key == K_DOWN:
                        x2change=0
            global x1,x2,y,x,z,l,x1change,x2change,xchange,ychange,player1score,player2score
            x1+=x1change
            x2+=x2change
            if x1<0:
                x1=0
            if x2 <0:
                x2 = 0
            if x1 >480:
                x1 = 480
            if x2 >480:
                x2=480
            pygame.draw.rect(screen,black,(640,0,10,640))
            x+=xchange
            y+=ychange
            pygame.draw.circle(screen,aqua,(x,y),30,30)
            if x<40:
                if y in range(x1,x1+160):
                    xchange=-xchange
                else:
                    player2score+=1
                    print("Player 1:", player1score, " | Player 2:", player2score)
                    x=640
                    y=random.randint(20, 620)
            if y<30:
                ychange=-ychange
            if x>1240:
                if y in range(x2,x2+160):
                    xchange =-xchange
                else:
                    player1score+=1
                    print("Player 1:", player1score, " | Player 2:", player2score)
                    x=640
                    y=random.randint(20, 620)
            if y>610:
                ychange = -ychange
            pygame.draw.rect(screen,red,(0, x1,10,160))
            pygame.draw.rect(screen,blue,(1270, x2,10,160))
    while True:
        screen.fill(white)
        game()
        while player1score >= 15:
            screen.fill(black)
            show_text("Player 1 Wins!", 620, 320,red)
            pygame.display.update()
            time.sleep(3)
            player1score=0
            player2score=0
            Pong()
        while player2score >= 15:
            screen.fill(black)
            show_text("Player 2 Wins!", 620, 320,blue)
            pygame.display.update()
            time.sleep(3)
            player1score=0
            player2score=0
            Pong()
        pygame.display.update()
Pong()
