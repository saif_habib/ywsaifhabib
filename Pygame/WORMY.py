import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((1000, 800))
global up,down,left,right,foodx,foody,snakex,snakey,score,segment,speed
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
black = (0,0,0)
global up,down,left,right,foodx,foody,snakex,snakey,score,segment,fontobj,msgobj,speed
def Wormy():
    global up,down,left,right,foodx,foody,snakex,snakey,score,segment,fontobj,msgobj,speed, snakelist, snakex, snakey
    up=0
    down=0
    right=0
    left=0
    score=0
    speed=180
    snakelist=[]
    pygame.display.set_caption("IMPOSSIBLE WORMY                             By: Saif Habib")
    foodx = (random.randint(0,800)//10)*10
    foody = (random.randint(0,800)//10)*10
    snakex = (random.randint(0,800)//10)*10
    snakey = (random.randint(0,800)//10)*10
    time.sleep(0.5)
    def show_text(msg,xx,yy,color):
        fontobj = pygame.font.SysFont("freesans",32)
        msgobj = fontobj.render(msg,False,color)
        screen.blit(msgobj,(xx,yy))
    def controlls():
        for event in pygame.event.get():
            if event.type == KEYUP:
                pass
            elif event.type == KEYDOWN:
                global up,down,left,right,foodx,foody,snakex,snakey,score,segment,speed
                if event.key == K_DOWN and not(up):
                    up=0
                    down=1
                    right=0
                    left=0
                if event.key == K_UP and not(down):
                    up=1
                    down=0
                    right=0
                    left=0
                if event.key == K_RIGHT and not(left):
                    up=0
                    down=0
                    right=1
                    left=0
                if event.key == K_LEFT and not(right):
                    up=0
                    down=0
                    right=0
                    left=1
        if down==1 and right ==0 and up == 0 and left == 0:
            snakey=snakey+10
        if down==0 and right ==1 and up == 0 and left == 0:
            snakex=snakex+10
        if down==0 and right ==0 and up == 1 and left == 0:
            snakey=snakey-10
        if down==0 and right ==0 and up == 0 and left == 1:
            snakex=snakex-10
    def eat():
        global up,down,left,right,foodx,foody,snakex,snakey,score,segment,speed
        if foodx == snakex and foody == snakey:
            foodx = (random.randint(0,800)//10)*10
            foody = (random.randint(0,800)//10)*10
            score+=5
            speed+=10
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            snakelist.insert(0,[snakex , snakey])
            controlls()
        else:
            pygame.draw.rect(screen,red,(foodx,foody,10,10))
    fpsclock = pygame.time.Clock()
    def die():
        if snakex>990 or snakey>990 or snakex<0 or snakey<0:
            up=0
            down=0
            right=0
            left=0
            print(score, "is score")
            time.sleep(0.5)
            Wormy()
        if snakex>990 or snakey>790 or snakex<0 or snakey<0:
            up=0
            down=0
            right=0
            left=0
            print(score, "is score")
            time.sleep(0.5)
            Wormy()
        elif [snakex,snakey] in snakelist[1:]:
            up=0
            down=0
            right=0
            left=0
            print(score, "is score")
            time.sleep(0.5)
            Wormy()
    while True:
        controlls()
        die()
        eat()
        snakelist.insert(0,[snakex , snakey])
        for segment in snakelist:
            pygame.draw.rect(screen,green,segment+[10,10])
            pygame.draw.rect(screen,green,segment+[10,10])
            pygame.draw.rect(screen,green,segment+[10,10])
            pygame.draw.rect(screen,green,segment+[10,10])
            pygame.draw.rect(screen,green,segment+[10,10])
        snakelist.pop()
        show_text(str(score),20,20,white)
        pygame.display.update()
        fpsclock.tick(speed)
        screen.fill(black)
Wormy()
