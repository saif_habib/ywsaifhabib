import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((640,640))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
black = (0,0,0)
pygame.display.set_caption("Tic-Tac-Toe!!!                              By: Saif Habib")
turn = 0
xpoints = 0
opoints = 0
global xpoints
global opoints
def menu():
    pygame.draw.rect(screen,blue,(5,5,65,50))
    pygame.draw.rect(screen,red,(565,5,65,50))
    show_text("Play",7,10,black)
    show_text("Quit",568,10,black)
    show_text("Tic Tac Toe!!! By: Saif Habib",110,10,green)
    pygame.display.update()
def tictactoe():
    def show_text(msg,x,y,color):
        fontobj=pygame.font.SysFont("freesans",32)
        msgobj=fontobj.render(msg,False,color)
        screen.blit(msgobj,(x,y))
    def Board():
        x=0
        y=0
        for sss in range(0,4,1):
            for ss in range(0,4,1):
                pygame.draw.rect(screen,white,(x,y,213,213),5)
                x+=213
            y+=213
            x=0
        pygame.display.update()
    turn = 0
    def Square1():
        global turn
    #print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=0 and y>=0 and x<=213 and y<=213:
            if turn==0 and TTT[1] == "_":
                pygame.draw.line(screen,red,(0,0),(213,213),5)
                pygame.draw.line(screen,red,(213,0),(0,213),5)
                turn = 1
                TTT[1] = "X"
            elif turn==1 and TTT[1] == "_":
                pygame.draw.circle(screen,blue,(106,106),106,5)
                turn = 0
                TTT[1] = "O"
    def Square2():
        global turn
    #print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=213 and y>=0 and x<=426 and y<=213:
            if turn==0 and TTT[2] == "_":
                pygame.draw.line(screen,red,(213,0),(426,213),5)
                pygame.draw.line(screen,red,(426,0),(213,213),5)
                turn = 1
                TTT[2] = "X"
            elif turn==1 and TTT[2] == "_":
                pygame.draw.circle(screen,blue,(319,106),106,5)
                turn = 0
                TTT[2] = "O"
    def Square3():
        global turn
    # print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=426 and y>=0 and x<=640 and y<=213:
            if turn==0 and TTT[3] == "_":
                pygame.draw.line(screen,red,(426,0),(640,213),5)
                pygame.draw.line(screen,red,(640,0),(426,213),5)
                turn = 1
                TTT[3] = "X"
            elif turn==1 and TTT[3] == "_":
                pygame.draw.circle(screen,blue,(532,106),106,5)
                turn=0
                TTT[3] = "O"
    def Square4():
        global turn
    #  print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=0 and y>=213 and x<=213 and y<=426:
            if turn==0 and TTT[4] == "_":
                pygame.draw.line(screen,red,(0,213),(213,426),5)
                pygame.draw.line(screen,red,(213,213),(0,426),5)
                turn = 1
                TTT[4] = "X"
            elif turn==1 and TTT[4]== "_":
                pygame.draw.circle(screen,blue,(106,320),106,5)
                turn = 0
                TTT[4] = "O"
    def Square5():
        global turn
    #   print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=213 and y>=213 and x<=426 and y<=426:
            if turn==0 and TTT[5] == "_":
                pygame.draw.line(screen,red,(213,213),(426,426),5)
                pygame.draw.line(screen,red,(426,213),(213,426),5)
                turn=1
                TTT[5] = "X"
            elif turn==1 and TTT[5] == "_":
                pygame.draw.circle(screen,blue,(320,320),106,5)
                turn=0
                TTT[5] = "O"
    def Square6():
        global turn
    # print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=426 and y>=213 and x<=640 and y<=426:
            if turn==0 and TTT[6] == "_":
                pygame.draw.line(screen,red,(426,213),(640,426),5)
                pygame.draw.line(screen,red,(640,213),(426,426),5)
                turn=1
                TTT[6] = "X"
            elif turn==1 and TTT[6] == "_":
                pygame.draw.circle(screen,blue,(534,320),106,5)
                turn=0
                TTT[6] = "O"
    def Square7():
        global turn
    #    print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=0 and y>=426 and x<=213 and y<=640:
            if turn==0 and TTT[7] == "_":
                pygame.draw.line(screen,red,(0,426),(213,640),5)
                pygame.draw.line(screen,red,(213,426),(0,640),5)
                turn=1
                TTT[7] = "X"
            elif turn == 1 and TTT[7] == "_":
                pygame.draw.circle(screen,blue,(106,534),106,5)
                turn=0
                TTT[7] = "O"
    def Square8():
        global turn
    #  print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=213 and y>=426 and x<=426 and y<=640:
            if turn==0 and TTT[8] == "_":
                pygame.draw.line(screen,red,(213,426),(426,640),5)
                pygame.draw.line(screen,red,(426,426),(213,640),5)
                turn=1
                TTT[8] = "X"
            elif turn==1 and TTT[8] == "_":
                pygame.draw.circle(screen,blue,(320,534),106,5)
                turn=0
                TTT[8] = "O"
    def Square9():
        global turn
    # print(turn)
        if x >= 565 and y >= 5 and x <= 630 and y <= 55:
            pygame.quit()
            exit()
        if x>=426 and y>=426 and x<=640 and y<=640:
            if turn==0 and TTT[9] == "_":
                pygame.draw.line(screen,red,(426,426),(640,640),5)
                pygame.draw.line(screen,red,(640,426),(426,640),5)
                turn=1
                TTT[9] = "X"
            elif turn==1 and TTT[9] == "_":
                pygame.draw.circle(screen,blue,(534,534),106,5)
                turn=0
                TTT[9] = "O"
    Board()
    #TTT = {1: "_", 2: "_", 3: "_", 4: "_", 5: "_", 6: "_", 7: "_", 8: "_", 9: "_"}
    #x_wins = TTT[1] == TTT[2] == TTT[3] == "X" or TTT[1] == TTT[5] == TTT[9] == "X" or TTT[1] == TTT[4] == TTT[7] == "X" or TTT[2] == TTT[5]== TTT[8] == "X" or TTT[3] == TTT[6] == TTT[9] == "X" or TTT[4] == TTT[5] == TTT[6] == "X" or TTT[3] == TTT[5] == TTT[7] == "X" or TTT[7] == TTT[8] == TTT[9] == "X"
    #o_wins = TTT[1] == TTT[2] == TTT[3] == "O" or TTT[1] == TTT[5] == TTT[9] == "O" or TTT[1] == TTT[4] == TTT[7] == "O" or TTT[2] == TTT[5]== TTT[8] == "O" or TTT[3] == TTT[6] == TTT[9] == "O" or TTT[4] == TTT[5] == TTT[6] == "O" or TTT[3] == TTT[5] == TTT[7] == "O" or TTT[7] == TTT[8] == TTT[9] == "O"
    def WINS():
        global TTT
        global turn
        x_wins = TTT[1] == TTT[2] == TTT[3] == "X" or TTT[1] == TTT[5] == TTT[9] == "X" or TTT[1] == TTT[4] == TTT[7] == "X" or TTT[2] == TTT[5]== TTT[8] == "X" or TTT[3] == TTT[6] == TTT[9] == "X" or TTT[4] == TTT[5] == TTT[6] == "X" or TTT[3] == TTT[5] == TTT[7] == "X" or TTT[7] == TTT[8] == TTT[9] == "X"
        o_wins = TTT[1] == TTT[2] == TTT[3] == "O" or TTT[1] == TTT[5] == TTT[9] == "O" or TTT[1] == TTT[4] == TTT[7] == "O" or TTT[2] == TTT[5]== TTT[8] == "O" or TTT[3] == TTT[6] == TTT[9] == "O" or TTT[4] == TTT[5] == TTT[6] == "O" or TTT[3] == TTT[5] == TTT[7] == "O" or TTT[7] == TTT[8] == TTT[9] == "O"
        if not x_wins and not o_wins and "_" not in TTT.values():
            print("TIE!!!")
            print("THANK YOU FOR PLAYING!")
            show_text("Tie!",10,600,green)
            pygame.display.update()
            TTT = {1: "_", 2: "_", 3: "_", 4: "_", 5: "_", 6: "_", 7: "_", 8: "_", 9: "_"}
            turn = 0
            time.sleep(1)
            screen.fill(black)
            menu()
            turn = 0
        elif x_wins or o_wins:
            if x_wins:
                global xpoints
                xpoints+=1
                print("X WINS!!!")
                print("Score: X = ",xpoints, "O = ", opoints, ".")
                show_text("X Wins!!!",10,600,red)
                pygame.display.update()
                TTT = {1: "_", 2: "_", 3: "_", 4: "_", 5: "_", 6: "_", 7: "_", 8: "_", 9: "_"}
                turn= 0
                time.sleep(1)
                screen.fill(black)
                menu()
                turn = 0
            elif o_wins:
                global opoints
                opoints+=1
                print("O WINS!!!")
                print("Score: X = ",xpoints, "O = ", opoints, ".")
                show_text("O Wins!!!",10,600,blue)
                pygame.display.update()
                TTT = {1: "_", 2: "_", 3: "_", 4: "_", 5: "_", 6: "_", 7: "_", 8: "_", 9: "_"}
                turn = 0
                time.sleep(1)
                screen.fill(black)
                menu()
                turn = 0
            turn = 0
    #TTT = {1: "_", 2: "_", 3: "_", 4: "_", 5: "_", 6: "_", 7: "_", 8: "_", 9: "_"}
    while True:
        Board()
        WINS()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                x,y=event.pos
                Square1()
                Square2()
                Square3()
                Square4()
                Square5()
                Square6()
                Square7()
                Square8()
                Square9()
                pygame.display.update()
def show_text(msg,x,y,color):
    fontobj=pygame.font.SysFont("freesans",32)
    msgobj=fontobj.render(msg,False,color)
    screen.blit(msgobj,(x,y))
    pygame.display.update()
TTT = {1: "_", 2: "_", 3: "_", 4: "_", 5: "_", 6: "_", 7: "_", 8: "_", 9: "_"}
while True:
    menu()
    for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                x,y=event.pos
                if x >= 5 and y >= 5 and x<=70 and y <= 55:
                    turn = 0
                    tictactoe()
                elif x >= 565 and y >= 5 and x <= 630 and y <= 55:
                    pygame.quit()
                    exit()
