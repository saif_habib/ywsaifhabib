import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((640,640))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
black = (0,0,0)
ccc = 85
cccc = 170
ccccc = 254
global clr,clrr,clrrr,up,down,left,right,foodx,foody,snakex,snakey,score,segment,fontobj,msgobj
up=0
down=0
right=0
left=0
score=0
speed=20
snakelist=[]
pygame.display.set_caption("SNAKE                             By: Saif Habib")
foodx = (random.randint(0,640)//20)*20
foody = (random.randint(0,640)//20)*20
snakex = (random.randint(0,640)//20)*20
snakey = (random.randint(0,640)//20)*20
def show_text(msg,xx,yy,color):
    fontobj = pygame.font.SysFont("freesans",32)
    msgobj = fontobj.render(msg,False,color)
    screen.blit(msgobj,(xx,yy))
def controls():
    for event in pygame.event.get():
        if event.type == KEYUP:
            pass
        elif event.type == KEYDOWN:
            global up,down,left,right,foodx,foody,snakex,snakey,score,segment
            if event.key == K_DOWN and not(up):
                up=0
                down=1
                right=0
                left=0
            if event.key == K_UP and not(down):
                up=1
                down=0
                right=0
                left=0
            if event.key == K_RIGHT and not(left):
                up=0
                down=0
                right=1
                left=0
            if event.key == K_LEFT and not(right):
                up=0
                down=0
                right=0
                left=1
    if down==1 and right ==0 and up == 0 and left == 0:
        snakey=snakey+20
    if down==0 and right ==1 and up == 0 and left == 0:
        snakex=snakex+20
    if down==0 and right ==0 and up == 1 and left == 0:
        snakey=snakey-20
    if down==0 and right ==0 and up == 0 and left == 1:
        snakex=snakex-20
def eat():
    global up,down,left,right,foodx,foody,snakex,snakey,score,segment,speed,clr,clrr,clrrr
    if foodx == snakex and foody == snakey:
        foodx = (random.randint(0,640)//20)*20
        foody = (random.randint(0,640)//20)*20
        score+=1
        clr = random.randint(0,255)
        clrr = random.randint(0,255)
        clrrr = random.randint(0,255)
        if score/10 in range(1,100):
            speed+=5
        snakelist.insert(0,[snakex , snakey])
        controls()
    else:
        cc = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
        pygame.draw.rect(screen,cc,(foodx,foody,20,20))
fpsclock = pygame.time.Clock()
clr = random.randint(0,255)
clrr = random.randint(0,255)
clrrr = random.randint(0,255)
xxx=0
def game():
    global up,down,left,right,foodx,foody,snakex,snakey,score,segment,speed,clr,clrr,clrrr, xxx, ccc, cc, c, ccccc, cccc
    while not snakex>630 or snakey>630 or snakex<0 or snakey<0 or [snakex,snakey] in snakelist[1:]:
        while xxx!=3:
            snakelist.insert(0,[snakex , snakey])
            xxx+=1
        controls()
        eat()
        show_text(str(score),20,20,(clr,clrr,clrrr))
        snakelist.insert(0,[snakex , snakey])
        for segment in snakelist:
            c=(random.randint(100,255),random.randint(100,255),random.randint(100,255))
            pygame.draw.rect(screen,c,segment+[20,20])
        snakelist.pop()
        pygame.display.update()
        fpsclock.tick(speed)
        if ccc < 86:
            ccc+=1
        else:
            ccc = 0
        if cccc < 86:
            cccc+=1
        else:
            cccc = 0
        if ccccc < 86:
            ccccc+=1
        else:
            ccccc = 0
        screen.fill((ccc,cccc,ccccc))
    if snakex>630 or snakey>630 or snakex<0 or snakey<0 or [snakex,snakey] in snakelist[1:]:
        exit()
game()
