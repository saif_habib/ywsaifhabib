import pygame
from pygame.locals import *
import random
import time
pygame.init()
screen = pygame.display.set_mode((640,640))
pygame.display.set_caption("Mario Animation!!!                               By: Saif Habib")
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
white = (255,255,255)
black = (0,0,0)
standleft = pygame.image.load('MarioLookLeft.png')
standright = pygame.image.load('MarioLookRight.png')
walkright1 = pygame.image.load('MarioWalkRight1.png')
walkright2 = pygame.image.load('MarioWalkRight2.png')
walkright3 = pygame.image.load('MarioWalkRight3.png')
walkleft1 = pygame.image.load('MarioWalkLeft1.png')
walkleft2 = pygame.image.load('MarioWalkLeft2.png')
walkleft3 = pygame.image.load('MarioWalkLeft3.png')
crouchleft = pygame.image.load('MarioSquatLeft.png')
crouchright = pygame.image.load('MarioSquatRight.png')
x=240
screen.fill(white)
pygame.display.update()
facingleft=False
facingright=False
def walkleft():
    global x,facingleft,facingright
    if not x <= 40:
        screen.fill(white)
        screen.blit(standleft,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x-=9
        screen.blit(walkleft1,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x-=9
        screen.blit(walkleft2,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x-=9
        screen.blit(walkleft3,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x-=9
        screen.blit(walkleft1,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x-=9
        screen.blit(walkleft2,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x-=9
        screen.blit(walkleft3,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x-=9
        screen.blit(standleft,(x,320))
        pygame.display.update()
        facingleft=True
        facingright=False
def walkright():
    global x,facingleft,facingright
    if not x >= 540:
        screen.fill(white)
        screen.blit(standright,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x+=9
        screen.blit(walkright1,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x+=9
        screen.blit(walkright2,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x+=9
        screen.blit(walkright3,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x+=9
        screen.blit(walkright1,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x+=9
        screen.blit(walkright2,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x+=9
        screen.blit(walkright3,(x,320))
        pygame.display.update()
        time.sleep(0.15)
        screen.fill(white)
        x+=9
        screen.blit(standright,(x,320))
        pygame.display.update()
        facingleft=False
        facingright=True
def squatleft():
    screen.fill(white)
    screen.blit(crouchleft,(x,320))
    time.sleep(0.3)
    pygame.display.update()
    screen.fill(white)
    screen.blit(standleft,(x,320))
    time.sleep(0.15)
    pygame.display.update()
def squatright():
    screen.fill(white)
    screen.blit(crouchright,(x,320))
    time.sleep(0.3)
    pygame.display.update()
    screen.fill(white)
    screen.blit(standright,(x,320))
    time.sleep(0.15)
    pygame.display.update()
left=0
right=0
downleft=0
downright=0
start=0
while True:
    for event in pygame.event.get():
        if event.type==KEYDOWN:
            if event.key == K_a:
                left=1
                start=1
            if event.key == K_d:
                right=1
                start=1
            if event.key==K_s and start==1:
                if facingleft==True:
                    downleft=1
                if facingright==True:
                    downright=1
        if event.type == KEYUP:
            left=0
            right=0
            downright=0
            downleft=0
    if left==1:
        walkleft()
    if right==1:
        walkright()
    if downleft==1:
        squatleft()
    if downright==1:
        squatright()
    screen.fill(white)
