import pygame
from pygame.locals import *
import random
import time
pygame.init()
global hit_list, player_list, block_list, bullet_list
screen = pygame.display.set_mode((800,600))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
aqua = (0,255,215)
white = (255,255,255)
black = (0,0,0)
pygame.display.set_caption("Shooting Game!")
player_list=pygame.sprite.Group()
block_list=pygame.sprite.Group()
bullet_list=pygame.sprite.Group()
class Block(pygame.sprite.Sprite):
    def __init__(self,color):
        super().__init__()
        self.image = pygame.Surface([5,10])
        self.image.fill(color)
        self.rect = self.image.get_rect()
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([20,10])
        self.image.fill(blue)
        self.rect = self.image.get_rect()
    def update(self):
        pos = pygame.mouse.get_pos()
        self.rect.x = pos[0]
class Bullet(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([20,10])
        self.image.fill(green)
        self.rect = self.image.get_rect()
    def update(self):
        self.rect.y-=5
for i in range(200):
    block = Block(red)
    block.rect.x = random.randrange(800)
    block.rect.y = random.randrange(550)
    block_list.add(block)
    player_list.add(block)
player=Player()
player_list.add(player)
player.rect.y = 570
while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            bullet = Bullet()
            bullet.rect.x = player.rect.x
            bullet.rect.y = player.rect.y
            bullet_list.add(bullet)
            player_list.add(bullet)
    player_list.update()
    for bullet in bullet_list:
        hit_list = pygame.sprite.spritecollide(bullet,block_list,True)
        for block in hit_list:
            bullet_list.remove(bullet)
            player_list.remove(bullet)
        if bullet.rect.y <= 0:
            bullet = Bullet()
            bullet.rect.x = player.rect.x
            bullet.rect.y = player.rect.y
            bullet_list.remove(bullet)
            player_list.remove(bullet)
    player_list.draw(screen)
    pygame.display.update()
    screen.fill(black)
