class Supermarket:
    name = "IKEA"
    def __init__(self,marketname):
        self.marketname = marketname;
        self.timings = "6:00 AM to 10:00 PM"
        self.employees = 30
        self.maxmoney = 1000
        self.items = {"milk":10, "plastic cheese":50, "cookies":10}
    def buyitem(self,itemname,itemcost):
        if self.maxmoney-itemcost>0:
            self.items[itemname] = itemcost
            self.maxmoney-=itemcost
            print("You have bought", itemname, "for", itemcost, ".")
        else:
            print("EXPIRED")
            exit()
    def returnitem(self,itemname,itemcost):
        if self.maxmoney+itemcost<1000 and itemname != milk:
            del self.items[itemname]
            self.maxmoney+=itemcost
            print("You have returned", itemname, "for", itemcost, ".")
        else:
            print("EXPIRED")
            exit()
