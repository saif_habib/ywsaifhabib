import pygame
from pygame.locals import *
import random
import time
pygame.init()
global ball1, xchange1, ychange1
screen = pygame.display.set_mode((960,960))
red = (255,0,0)
blue = (0,0,255)
green = (0,255,0)
aqua = (0,255,215)
white = (255,255,255)
black = (0,0,0)
player1score = 0
player2score = 0
pygame.display.set_caption("Pong!!!                               By: Saif Habib")
class Ball:
    def __init__(self,x,y,color,radius):
        self.color = color
        self.radius = radius
        self.x = x
        self.y = y
    def draw(self):
        pygame.draw.circle(screen,self.color,(self.x, self.y),self.radius)
    def move(self,xchange,ychange):
        self.x = self.x + xchange
        self.y = self.y + ychange
class Paddle:
    def __init__(self,x,y,color,width,height):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
    def draw(self):
        pygame.draw.rect(screen,self.color,(self.x,self.y,self.width,self.height))
    def move(self,ychange):
        self.y = self.y + ychange
def show_text(msg,xx,yy,color):
    fontobj = pygame.font.SysFont("freesans",32)
    msgobj = fontobj.render(msg,False,color)
    screen.blit(msgobj,(xx,yy))
ball1=Ball(150,150,green,20)
xchange1 = random.randint(-2,2)
ychange1 = random.randint(-2,2)
ball2=Ball(810,810,white,20)
xchange2 = random.randint(-2,2)
ychange2 = random.randint(-2,2)
paddle1=Paddle(30,320,red,8,100)
paddle2=Paddle(930,320,blue,8,100)
paddle1change = 0
paddle2change = 0
while True:
    ball1.draw()
    ball2.draw()
    paddle1.draw()
    paddle2.draw()
    ball1.move(xchange1,ychange1)
    ball2.move(xchange2,ychange2)
    paddle1.move(paddle1change)
    paddle2.move(paddle2change)
    pygame.display.update()
    screen.fill(black)
    if paddle1.y < 0:
        paddle1.y = 0
    if paddle1.y > 860:
        paddle1.y = 860
    if paddle2.y < 0:
        paddle2.y = 0
    if paddle2.y > 860:
        paddle2.y = 860
    if ball1.x>960:
        xchange1=-xchange1
    if ball1.x<0:
        xchange1=-xchange1
    if ball1.y>960:
        ychange1=-ychange1
    if ball1.y<0:
        ychange1=-ychange1
    if ball2.x>960:
        xchange2=-xchange2
    if ball2.x<0:
        xchange2=-xchange2
    if ball2.y>960:
        ychange2=-ychange2
    if ball2.y<0:
        ychange2=-ychange2
    if ball1.x <= paddle1.x+21 and paddle1.y <= ball1.y <= paddle1.y+100:
        xchange1=-xchange1
    if ball1.x >= paddle2.x-21 and paddle2.y <= ball1.y <= paddle2.y+100:
        xchange1=-xchange1
    if ball2.x <= paddle1.x+21 and paddle1.y <= ball2.y <= paddle1.y+100:
        xchange2=-xchange2
    if ball2.x >= paddle2.x-21 and paddle2.y <= ball2.y <= paddle2.y+100:
        xchange2=-xchange2
    if ball1.x-20 <= ball2.x <= ball1.x+20 and ball1.y-20 <= ball2.y <= ball1.y+20:
        xchange1 = random.randint(-2,2)
        ychange1 = random.randint(-2,2)
        xchange2 = random.randint(-2,2)
        ychange2 = random.randint(-2,2)
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_w:
                paddle1change=-8
            elif event.key == K_s:
                paddle1change=8
            if event.key == K_UP:
                paddle2change=-8
            elif event.key == K_DOWN:
                paddle2change=8
        if event.type == KEYUP:
            if event.key == K_w:
                paddle1change=0
            elif event.key == K_s:
                paddle1change=0
            elif event.key == K_UP:
                paddle2change=0
            elif event.key == K_DOWN:
                paddle2change=0
    if ball1.x < paddle1.x:
        player2score+=1
        ball1=Ball(300,300,green,20)
        xchange1 = random.randint(1,4)
        ychange1 = random.randint(1,4)
    if ball1.x > paddle2.x:
        player1score+=1
        ball1=Ball(460,460,green,20)
        xchange1 = random.randint(1,4)
        ychange1 = random.randint(1,4)
    if ball2.x < paddle1.x:
        player2score+=1
        ball2=Ball(480,480,white,20)
        xchange2 = random.randint(2,4)
        ychange2 = random.randint(2,4)
    if ball2.x > paddle2.x:
        player1score+=1
        ball2=Ball(480,480,white,20)
        xchange2 = random.randint(2,4)
        ychange2 = random.randint(2,4)
    show_text("Player 1: "+str(player1score), 10,10,red)
    show_text("Player 2: "+str(player2score), 780,10,blue)
