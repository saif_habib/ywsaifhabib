class Zoo:
    name = "This is a Zoo"
    def __init__(self,zooname):
        self.zooname = zooname;
        self.timings = "Open 9:00 AM to 5:00 PM"
        self.max_animals = 50
        self.animals={}
    def addAnimal(self,animal_name,animal_number):
        if self.max_animals-animal_number > 0:
            self.animals[animal_name] = animal_number
            self.max_animals-=animal_number
            print(self.animals, "are the animals")
            print(self.max_animals, "is amount of animals")
        elif self.max_animals-animal_number <= 0:
            print("ERROR 404 NUMBER NOT AVAILABLE")
            exit()
    def removeAnimal(self,animal_name):
        if animal_name in self.animals:
            self.max_animals+=self.animals[animal_name]
            del self.animals[animal_name]
            print(self.animals, "are the animals")
            print(self.max_animals, "is amount of animals")
        else:
            print("ERROR 404 NAME NOT FOUND")
            exit()
