import random
class BANK:
  def __init__(self,bankname):
    self.bankname = bankname;
    self.timings = "24/7"
    self.account_id = random.randint(1000,1384601581823170000)
    self.min_money = 0
    self.max_money = 1000000000
    self.amount = random.randint(0,10000)
  def deposit(self):
    print("How much money do you want to deposit?")
    money=input()
    money=int(money)
    if self.amount+money<=self.max_money:
      self.amount+=money
      print("$", self.amount, "left")
    else:
      print("Your deposit is too big. Try again.")
  def withdraw(self):
    print("How much money do you want to withdraw?")
    money=input()
    money=int(money)
    if self.amount-money>=self.min_money:
      self.amount-=money
      print("$", self.amount, "left")
    else:
      print("Your withdrawal is to big. Try Again.")
      
