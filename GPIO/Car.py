#Open In Terminal with command: sudo python3 Car.py
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(2,GPIO.OUT)
GPIO.setup(3,GPIO.OUT)
GPIO.setup(14, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
m1p1 = GPIO.PWM(2, 100)
m1p2 = GPIO.PWM(3, 100)
m2p1 = GPIO.PWM(14, 100)
m2p2 = GPIO.PWM(15, 100)
dc = 50
def Right():
   m2p1.start(dc)
   m2p2.stop()
   m1p1.stop()
   m1p2.stop()
def Left():
    m2p1.stop()
    m2p2.stop()
    m1p1.stop()
    m1p2.start(dc)
def Forward():
   m2p1.start(dc)
   m2p2.stop()
   m1p1.start(dc)
   m1p2.stop()
def Backward():
   m2p1.stop()
   m2p2.start(dc)
   m1p1.stop()
   m1p2.start(dc)
def Stop():
   m2p2.stop()
   m2p1.stop()
   m1p2.stop()
   m1p1.stop()
def Getch():
   import sys, tty, termios
   fd = sys.stdin.fileno()
   old_settings = termios.tcgetattr(fd)
   try:
      tty.setraw(sys.stdin.fileno())
      ch = sys.stdin.read(1)
   finally:
      termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
   return ch
while True:
   dc+=50
   a = Getch()
   if dc == 100:
      dc-= 30
   elif dc == 0:
      dc += 50
   print("I is accelerate, K is decelerate, D is right, A is left, S is backwards, W is forwards, Q is stop, E is cleanup.")
   if a == "d":
      Right()
   elif a == "a":
      Left()
   elif a == "w":
      Forward()
   elif a == "s":
      Backward()
   elif a == "q":
      Stop()
   elif a == "e":
      GPIO.cleanup() 
   elif a == "i" and dc!= 100:
      dc+=10
   elif a == "k" and dc+= 0:
      dc-=10
   else:
      pass
