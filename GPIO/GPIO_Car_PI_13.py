#Open In Terminal with command: sudo python3 Car.py
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(2,GPIO.OUT)
GPIO.setup(3,GPIO.OUT)
GPIO.setup(14, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
def Right():
   GPIO.output(14, GPIO.HIGH)
   GPIO.output(15, GPIO.LOW)
   GPIO.output(2, GPIO.LOW)
   GPIO.output(3, GPIO.LOW)
def Left():
    GPIO.output(14, GPIO.HIGH)
    GPIO.output(15, GPIO.HIGH)
    GPIO.output(2, GPIO.HIGH)
    GPIO.output(3, GPIO.LOW)
def Forward():
   GPIO.output(14, GPIO.HIGH)
   GPIO.output(15, GPIO.LOW)
   GPIO.output(2, GPIO.HIGH)
   GPIO.output(3, GPIO.LOW)
def Backward():
   GPIO.output(14, GPIO.LOW)
   GPIO.output(15, GPIO.HIGH)
   GPIO.output(2, GPIO.LOW)
   GPIO.output(3, GPIO.HIGH)
def Stop():
   GPIO.output(14, GPIO.HIGH)
   GPIO.output(15, GPIO.HIGH)
   GPIO.output(2, GPIO.HIGH)
   GPIO.output(3, GPIO.HIGH)
def Getch():
   import sys, tty, termios
   fd = sys.stdin.fileno()
   old_settings = termios.tcgetattr(fd)
   try:
      tty.setraw(sys.stdin.fileno())
      ch = sys.stdin.read(1)
   finally:
      termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
   return ch
while True:
   a = Getch()
   print("D is right, A is left, S is backwards, W is forwards, Q is cleanup.")
   if a == "d":
      Right()
   elif a == "a":
      Left()
   elif a == "w":
      Forward()
   elif a == "s":
      Backward()
   elif a == "q":
      Stop()
   elif a == "e":
      GPIO.cleanup()
