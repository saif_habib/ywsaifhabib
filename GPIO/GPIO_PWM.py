import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(26, GPIO.OUT)
#---------------------------------------
#PICK POWER

def EX1():
    p=GPIO.PWM(16,100)
    p.start(DC) #DUTY CYCLE [POWER]
    time.sleep(1)
    p.stop()
    time.sleep(1)
    #unindent this while true
    while True:
        print("Enter a number between 1 and 100 for the Duty Cycle (POWER)")
        dc = input()
        DC = int(dc)
        if DC > 100:
            pass
        else:
            EX1()
            
#---------------------------------------
#DIM TO BRIGHT TO DIM

DC = 0
x = 1
a = 20
def EX2and3():
    p=GPIO.PWM(16,100)
    p.start(DC) #DUTY CYCLE [POWER]
    time.sleep(0.5)
    p.stop()
    time.sleep(0.5)
    #unindent this while true
    while True:
        if DC == 100:
            while a != 0:
                x=0
                DC-=5
                EX2()
                time.sleep(0.1)
                print(DC)
        elif x == 1:
            DC+=10
            EX2()
            time.sleep(0.1)
            print(DC)

#---------------------------------------
#3 Lights With Different Timing

def EX4():
    one = GPIO.PWM(16, 100)
    two = GPIO.PWM(12, 100)
    three = GPIO.PWM(26, 100)
    one.start(100)
    two.start(100)
    three.start(100)
    time.sleep(0.2)
    one.stop()
    time.sleep(0.2)
    two.stop()
    time.sleep(0.2)
    three.stop()
    one.start(0)
    two.start(0)
    three.start(0)
    time.sleep(0.2)
    one.stop()
    time.sleep(0.2)
    two.stop()
    time.sleep(0.2)
    three.stop()
while True:
    EX4()
