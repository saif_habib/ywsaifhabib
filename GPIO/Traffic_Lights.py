#Import And Setup
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(4, GPIO.OUT)      #   ]
GPIO.setup(17, GPIO.OUT)    #   |       ROAD A
GPIO.setup(22, GPIO.OUT)    #   ]    MAIN ROAD

GPIO.setup(12, GPIO.OUT)    #   ]
GPIO.setup(16, GPIO.OUT)    #   |        ROAD B
GPIO.setup(21, GPIO.OUT)    #   ]   OTHER ROAD

GPIO.setup(23, GPIO.IN)         #   ]        BUTTON

#==================================================
z=0
while True:
    #1 == button pressed
    x=GPIO.input(23)
    print(x)
    if x == 1:
        GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
        GPIO.output(12, GPIO.HIGH) #PR Green
        GPIO.output(4, GPIO.HIGH)   #FW Green
        GPIO.output(17, GPIO.HIGH) #FW Yellow
        GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
        GPIO.output(12, GPIO.HIGH) #PR Green
        time.sleep(1)
        if x == 1:
            GPIO.output(4, GPIO.HIGH)   #FW Green
            GPIO.output(17, GPIO.LOW) #FW Yellow [OFF]
            GPIO.output(22, GPIO.LOW) #FW Red [OFF]
            GPIO.output(12, GPIO.LOW) #PR Green  [OFF]
            GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
            GPIO.output(16, GPIO.HIGH) #PR Yellow
            time.sleep(1)
            GPIO.output(12, GPIO.LOW) #PR Green  [OFF]
            GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
            GPIO.output(21, GPIO.HIGH) #PR Red
            time.sleep(5)                           #Time to cross street
        elif x == 0:
            GPIO.output(4, GPIO.LOW)   #FW Green [OFF]
            GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
            GPIO.output(21, GPIO.LOW) #PR Red [OFF]
            GPIO.output(17, GPIO.LOW) #FW Yellow [OFF]
            GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]

    if x == 0:
        GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
        GPIO.output(12, GPIO.HIGH) #PR Green
        GPIO.output(4, GPIO.LOW)   #FW Green [OFF]
        GPIO.output(17, GPIO.HIGH)#FW Yellow
        GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
        time.sleep(1)
        GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
        GPIO.output(17, GPIO.LOW) #FW Yellow [OFF]
        GPIO.output(22, GPIO.HIGH)#FW Red
        GPIO.output(4, GPIO.LOW)   #FW Green [OFF]
        GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
        time.sleep(1)
        GPIO.output(12, GPIO.HIGH) #PR Green
        GPIO.output(16, GPIO.LOW) #PR Yellow [OFF]
        GPIO.output(21, GPIO.LOW) #PR Red [OFF]
