import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.IN)
Sensor = 23
GPIO.setup(18, GPIO.OUT)#RED
GPIO.setup(23, GPIO.OUT)#GREEN
while True:
    Sense = GPIO.input(21)
    if Sense == 0:
        print("Object Detected,", "pin number", Sensor)
        GPIO.output(23, GPIO.LOW)
        GPIO.output(18, GPIO.HIGH)
    else:
        print("No Object Detected.")
        GPIO.output(23, GPIO.HIGH)
        GPIO.output(18, GPIO.LOW)
