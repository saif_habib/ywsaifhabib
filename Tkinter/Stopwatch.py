import tkinter
from tkinter import *
import time
import random
master = Tk()
master.title("STOPWATCH")
text_x=StringVar()
h=0
m=0
s=0
STARTED=False
stay = False
display=(h,':',m,':',s)
label=Label(master,textvariable=text_x,font=('Roboto',50)).grid(row=0,column=0)
def stop():
    global stay,STARTED
    stay = True
    STARTED=False
def reset():
    global stay,h,m,s,STARTED,display
    h=0
    m=0
    s=0
    display=(h,':',m,':',s)
    text_x.set(display)
    stay=True
    STARTED=False
def START():
    global STARTED,stay
    stay=False
    STARTED=True
def print_text():
    global s,m,h,t,stay,STARTED
    if STARTED==True:
        if stay == False:
            s+=1
            if s==60:
                s=0
                m+=1
            if m==60:
                m=0
                h+=1
            display=(h,':',m,':',s)
            text_x.set(display)
            print(h,':',m,':',s)
        else:
            print("STOPPED")
    master.after(1000,print_text)
reset()
start=Button(master,text="START", command = START).grid(row=1,column=0)
stop=Button(master,text="STOP", command = stop).grid(row=2,column=0)
reset=Button(master,text="RESET", command = reset).grid(row=3, column=0)
print_text()
mainloop()
