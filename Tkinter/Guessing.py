import tkinter
from tkinter import *
import random
master = Tk()
master.title("Guess My Number")
def enter():
    global guessingnumber, chances
    x = guess.get()
    try:
        x=int(x)
        if x in range(1,100):
            if guessingnumber == x:
                right = "YOU ARE CORRECT!"
                guess.delete(0, END)
                guess.insert(END, right)
                chances+=1
                txt = "Chances:", chances
                Label(master, text = txt).grid(row=6, column = 1)
                Label(master, text = "-------------").grid(row=4, column =1)
                Label(master, text = "-------------").grid(row=3, column =1)
                instructions = Label(master, text = "                                                                                                                                   ").grid(row = 0, column = 1)
                instructions = Label(master, text = "Press the Restart button at the bottom.").grid(row = 0, column = 1)
            if guessingnumber <x:
                toohigh = "YOUR ANSWER IS TOO HIGH!"
                guess.delete(0, END)
                guess.insert(END, toohigh)
                chances+=1
                txt = "Chances:", chances
                Label(master, text = txt).grid(row=6, column = 1)
            if guessingnumber >x:
                toolow = "YOUR ANSWER IS TOO LOW!"
                guess.delete(0, END)
                guess.insert(END, toolow)
                chances+=1
                txt = "Chances:", chances
                Label(master, text = txt).grid(row=6, column = 1)
        else:
            guess.delete(0, END)
            guess.insert(END, "OUT OF RANGE")
    except ValueError:
        guess.delete(0, END)
        guess.insert(END, "ENTER A VALID NUMBER")
def reset():
    global guessingnumber, chances, enter, clear, instructions
    guessingnumber=random.randint(1,100)
    guess.delete(0,END)
    chances = 0
    instructions = Label(master, text = "Guess my number from 1 to 100! Type answer in box below.").grid(row = 0, column = 1)
    Label(master, text = txt).grid(row=6, column = 1)
    enter = Button(master, text = "ENTER", command = enter).grid(row = 3, column = 1)
    clear = Button(master, text = "CLEAR", command = clear).grid(row = 4, column = 1)
def clear():
    guess.delete(0,END)
chances = 0
guessingnumber = random.randint(1,100)
guess = Entry(master)
guess.grid(row=2, column = 0, columnspan = 9)
txt = "Chances:", chances
chance = Label(master, text = txt).grid(row=6, column = 1)
instructions = Label(master, text = "                                                                                                                                   ").grid(row = 0, column = 1)
instructions = Label(master, text = "Guess my number from 1 to 100! Type answer in box below.").grid(row = 0, column = 1)
enter = Button(master, text = "ENTER", command = enter).grid(row = 3, column = 1)
reset = Button(master, text = "RESTART", command = reset).grid(row = 5, column = 1)
clear = Button(master, text = "CLEAR", command = clear).grid(row = 4, column = 1)
mainloop()
