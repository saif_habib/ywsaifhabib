import tkinter
from tkinter import *
import time
root = Tk()
root.title("QUIZ")
correct=0
attemps=0
score=StringVar()
def q1():
    global a,var1, correct, attemps, score
    Label(root, textvariable = score).grid(row=0)
    Label(root, text = "Who came first?").grid(row=1)
    #ANSWER = 5
    a = IntVar()
    characters = [
        ("Darth Vader",1),
        ("Kylo Ren", 2),
        ("Darth Sidious", 3),
        ("Supreme Leader Snoke", 4),
        ("Darth Plageius", 5)
        ]
    def ShowChoice1():
        global a,var1, correct, attemps, score
        Label(root, textvariable = score).grid(row=0)
        var1=a.get()
        if var1 == 5:
            Label(root, text = "You are correct!").grid(row=7)
            correct+=1
            attemps+=1
            q2()
        else:
            Label(root, text = "You are wrong!").grid(row=7)
            attemps+=1
        score.set("You have a score of "+str(100*(correct/attemps))+"%.")
        Label(root, textvariable = score).grid(row=0)
    i=2
    for txt, val in characters:
        Radiobutton(root, text = txt, padx = 20, variable = a, command = ShowChoice1, value=val).grid(row=i)
        i+=1
def q2():
    global b, var2,correct, attemps, score
    Label(root, textvariable = score).grid(row=0)
    Label(root, text = "(12x-24 = 720) Solve for x.").grid(row=8)
    #Answer = 4
    b = IntVar()
    solutions = [
        ("48", 1),
        ("2", 2),
        ("60", 3),
        ("62", 4),
        ("72", 5)
        ]
    def ShowChoice2():
        global b, var2,correct, attemps, score
        Label(root, textvariable = score).grid(row=0)
        var2=b.get()
        if var2 == 4:
            Label(root, text = "You are correct!").grid(row=14)
            correct+=1
            attemps+=1
            q3()
        else:
            Label(root, text = "You are wrong!").grid(row=14)
            attemps+=1
        score.set("You have a score of "+str(100*(correct/attemps))+"%.")
        Label(root, textvariable = score).grid(row=0)
    k = 9
    for txt, val in solutions:
        Radiobutton(root, text = txt, padx = 20, variable = b, command = ShowChoice2, value=val).grid(row=k)
        k+=1
def q3():
    global c, var3,correct, attemps, score
    Label(root, textvariable = score).grid(row=0)
    Label(root, text = "Who won the Alabama senator election?").grid(row=15)
    #Answer = 3
    c = IntVar()
    candidates = [
        ("Logan Paul", 1),
        ("Donald Trump", 2),
        ("Doug Jones", 3),
        ("Roy Moore", 4),
        ("Hillary Clinton",5)
        ]
    def ShowChoice3():
        global c, var3,correct, attemps, score
        Label(root, textvariable = score).grid(row=0)
        var3 = c.get()
        if var3 == 3:
            Label(root, text = "You are correct!").grid(row=21)
            correct+=1
            attemps+=1
        else:
            Label(root, text = "You are wrong!").grid(row=21)
            attemps+=1 
        score.set("You have a score of "+str(100*(correct/attemps))+"%.")
        Label(root, textvariable = score).grid(row=0)
    l=16
    for txt, val in candidates:
        Radiobutton(root, text = txt, padx = 20, variable = c, command = ShowChoice3, value = val).grid(row=l)
        l+=1
q1()
mainloop()
