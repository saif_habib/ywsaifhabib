import tkinter
from tkinter import *
master = Tk()

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Delete both the triple quote under each title to make the code work.

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

#Line in middle of the canvas
'''
canvas_width=80
canvas_height=40
w=Canvas(master, width = canvas_width, height = canvas_height)
w.pack()
y=int(canvas_height/2)
w.create_line(0, y, canvas_width, y, fill="#476042")
'''

#Yellow rectangle with green border
'''
w=Canvas(master, width=200, height=100)
w.pack()
w.create_rectangle(50, 20, 150, 80, fill="#476042")
w.create_rectangle(65, 35, 135, 65, fill = "yellow")
w.create_line(0,0,50, 20, fill="#476042", width = 3)
w.create_line(0,100,50, 80, fill="#476042", width = 3)
w.create_line(150, 20, 200, 0, fill="#476042", width = 3)
w.create_line(150, 80, 200, 100, fill="#476042", width=3)
'''

#Text in the canvas
'''
canvas_width = 200
canvas_height = 100
w=Canvas(master, width = canvas_width, height = canvas_height)
w.pack()
w.create_text(canvas_width/2, canvas_height/2, text = "ME NAMES JEFF")
'''

#Circle
'''
canvas_width = 200
canvas_height = 150
w=Canvas(master, width = canvas_width, height = canvas_height)
w.pack()
w.create_oval(50, 50, 100, 100)
'''

#Drawing!
'''
cw=500
ch=150
def paint(event):
    c = "#476042"
    x1, y1 = (event.x-0.1), (event.y-0.1)
    x2, y2 = (event.x+0.1), (event.y+0.1)
    w.create_oval(x1, y1, x2, y2, fill = c)
w=Canvas(master, width=cw, height=ch)
w.pack(expand=YES, fill=BOTH)
w.bind("<B1-Motion>", paint)
message = Label(master, text = "Press and Drag the mouse to draw!")
message.pack(side = BOTTOM)
'''

#Polygon
'''
cw=200
ch=200
pg= "#476042"
w=Canvas(master, width=cw, height = ch)
w.pack()
points = [100, 140, 110, 110, 140, 100, 110, 90, 100, 60, 90, 90, 60, 100, 90, 110]
w.create_polygon(points, outline = pg, fill = 'yellow', width = 3)
'''

#Slider
'''
w=Scale(master, from_=0, to=42)
w.pack()
w=Scale(master, from_=0, to=200, orient = HORIZONTAL)
w.pack()
'''

mainloop()
