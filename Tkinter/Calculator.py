import tkinter
from tkinter import *
master = Tk()
master.title("Calculator")
solution = ''
output=StringVar()
e=Entry(master, textvariable=output).grid(row = 0,column=1, columnspan=4)
def click(number):
    global solution
    solution+=str(number)
    output.set(solution)
def enter():
    global solution,x
##    try:
##        x = str(eval(solution))
##        output.set(x)
##    except ZeroDivisionError:
##        output.set('Error')
    if "/0" in solution:
        x = "ERROR"
    if solution.count(".")>1:
        x = "ERROR"
    else:
        x = str(eval(solution))
        solution=x
    output.set(x)
def clear():
    global solution
    solution = ''
    output.set('')
one = Button(master, text = "1",command=lambda:click(1)).grid(row=1, column=1)
two = Button(master, text = "2",command=lambda:click(2)).grid(row=1, column=2)
three = Button(master, text = "3",command=lambda:click(3)).grid(row=1, column=3)
four = Button(master, text = "4",command=lambda:click(4)).grid(row=2, column=1)
five = Button(master, text = "5",command=lambda:click(5)).grid(row=2, column=2)
six = Button(master, text = "6",command=lambda:click(6)).grid(row=2, column=3)
seven = Button(master, text = "7",command=lambda:click(7)).grid(row=3, column=1)
eight = Button(master, text = "8",command=lambda:click(8)).grid(row=3, column=2)
nine = Button(master, text = "9",command=lambda:click(9)).grid(row=3, column=3)
zero = Button(master, text = "0",command=lambda:click(0)).grid(row=4, column=2)
add = Button(master, text = "+",command=lambda:click("+")).grid(row=1, column=4)
subtract = Button(master, text = "-",command=lambda:click("-")).grid(row=2, column=4)
multiply = Button(master, text = "x",command=lambda:click("*")).grid(row=3, column=4)
divide = Button(master, text = "÷",command=lambda:click("/")).grid(row=4, column=4)
decimal = Button(master, text = ".", command=lambda:click(".")).grid(row=5, column = 4)
clear = Button(master, text = "CLEAR ALL", command = clear).grid(row=6, column = 0, columnspan = 5)
equal = Button(master, text = "ENTER", command = enter).grid(row=5, column = 0, columnspan = 5)
mainloop()
