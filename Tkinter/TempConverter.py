import tkinter
from tkinter import *
master = Tk()
master.title("Temperature Converter")
text_x = StringVar()
text_y = StringVar()
def convertcfe():
    a=float(cfe.get())
    f=1.8*a+32
    text_x.set(f)
    print(f, "is the conversion of", a, "degrees Celsius to Fahrenheit.")
    cfe.delete(0,END)
def convertfce():
    b=float(fce.get())
    c=(b-32)/1.8
    text_y.set(c)
    print(c, "is the conversion of", b, "degrees Fahrenheit to Celsius.")
    fce.delete(0,END)
fc = Label(master, text="FAHRENHEIT TO CELSIUS").grid(row=1, column=0)
fce = Entry(master, font=("DroidSansMono",10))
fce.grid(row=2, column = 0)
fceconvert = Button(master,text="CONVERT FAHRENHEIT TO CELSIUS", command=convertfce).grid(row=3,column=0)
fcSHOW = Label(master, textvariable=text_y).grid(row=4, column=0)
cf = Label(master, text="CELSIUS TO FAHRENHEIT").grid(row=5, column=0)
cfe = Entry(master, font=("DroidSansMono",10))
cfe.grid(row=6, column = 0)
cfeconvert = Button(master,text="CONVERT CELSIUS TO FAHRENHEIT", command=convertcfe).grid(row=7,column=0)
cfSHOW = Label(master, textvariable=text_x).grid(row=8, column=0)
mainloop()
