import tkinter
from tkinter import *
from tkinter.messagebox import *
from tkinter.filedialog import askopenfilename
master = Tk()


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Delete both the triple quote under each title to make the code work.

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

#Dialog Boxes
'''
def answer():
    messagebox.showerror("Answer", "Sorry, no answer available")
def callback():
    if messagebox.askyesno('Verify', "Really quit?"):
        messagebox.showwarning("Yes", "Not yet implemented")
    else:
        messagebox.showinfo("No", "Quit has been cancelled")
Button(text ="Quit", command = callback).pack(fill = X)
Button(text ="Answer", command = answer).pack(fill = X)
'''

#Files
'''
def callback():
    name = askopenfilename()
    print(name)
errmsg = "Dude its an error ok"
Button(text = "File Locator. Click to find file. Displays location in file menu with search or clicking the file", command = callback).pack(fill=X)
'''

#Menu
'''
def NewFile():
    print("New File")
def OpenFile():
    name = askopenfilename()
    print(name)
def About():
    print("This is a simple example of a menu. Created Sunday the 28th of the first month, January, in the 2018th year in the 21st centuary in YoungWonks in Pleasanton by Saif Habib")
menu = Menu(master)
master.config(menu = menu)
filemenu=Menu(menu)
menu.add_cascade(label="File", menu = filemenu)
filemenu.add_command(label = "New", command = NewFile)
filemenu.add_command(label = "Open...", command = OpenFile)
filemenu.add_separator()
filemenu.add_command(label = "Exit", command=quit)
helpmenu = Menu(menu)
menu.add_cascade(label = "Help", menu = helpmenu)
helpmenu.add_command(label = "About", command = About)
'''

mainloop()
