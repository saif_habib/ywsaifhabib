#Graph Markers https://matplotlib.org/api/markers_api.html#
#In LXTerminal, run:
#'pip install --upgrade pip --user'
#then
#'pip3 install matplotlib --user'
#to install 'matplotlib'

import matplotlib
import matplotlib.pyplot as plt
import random
import csv



'''
#CSV Test
x=[]
y=[]
file=open("file.csv", "r")
plots=csv.reader(file, delimiter=",")
for row in plots:
    x.append(int(row[0]))
    y.append(int(row[1]))
plt.plot(x,y,label="Loaded from CSV file")
plt.xlabel("X")
plt.ylabel("Y")
plt.title("X vs Y")
plt.show()

#Newspapers
n=["The Sunday", "The Economic Times", "The Sport Star", "The Entertainent"]
s=[input(), input(), input(), input()]
t=["General News", "Finance", "Sports", "Movies"]
c=["blue", "green", "red", "orange"]
plt.pie(s,labels=t,colors=c,startangle=90,shadow=True,explode=(0,0.1,0,0),autopct="%1.1f%%")
plt.show()

#Movie ratings
b=[]
for a in range(0,31):
    c=random.randint(1,100)
    b.append(c)
b.sort()
plt.axis([0,30,0,10])
plt.hist([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30], bins=b, histtype="bar", color="green", rwidth=1, label="Movie ratings")
plt.show()

#Histograms
plt.axis([0,5,0,5])
plt.hist([1,2,1,3,4], bins=[1,2,3,4,5], histtype="bar", color="green", rwidth=0.8, label="Percentage of scores")
plt.show()

#Score distribution
s1=random.randint(50,100)
s2=random.randint(50,100)
s3=random.randint(50,100)
s4=random.randint(50,100)
s5=random.randint(50,100)
x=["student1", "student2", "student3", "student4", "student5"]
y=[s1,s2,s3,s4,s5]
plt.axis([0,5,0,100])
plt.bar(x,y,label="Students and scores",color="green")
plt.show()

#Bar graphs
x=[5,14,18,22,26]
y=[2,4,16,21,24]
a=[15,24,8,12,16]
b=[12,11,25,7,14]
plt.axis([0,30,0,30])
plt.bar(x,y,label="Bar graph",color="green")
plt.bar(a,b,label="Other bar graph",color="blue")
plt.show()

#File reading
a=open("file1.txt")
b=open("file2.txt")
x=a.read().split("\n")
y=b.read().split("\n")
a.close()
b.close()
x.append("0")
y.append("0")
x.append("50")
y.append("50")
x.sort()
y.sort()
plt.axis([0,50,0,50])
plt.title('File Plot')
plt.xlabel('file1')
plt.ylabel('file2')
plt.plot(x,y,'8')
plt.show()

#Square
#Ajustment of sizes of x-axis and y-axis is required
x=[-10,-10,10,10]
y=[-10,10,-10,10]
plt.plot(x,y,'8')
plt.axis([-11,11,-11,11])
plt.title('Square')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

#Circle
#Ajustment of sizes of x-axis and y-axis is required
for x in range(-10,11):
    y=((10**2-x**2)**0.5)
    plt.plot(x,y,'8')
    y=-((10**2-x**2)**0.5)
    plt.plot(x,y,'8')
plt.axis([-10,10,-10,10])
plt.title('Circle')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

#Quadratic equation
plt.axis([-10,10,0,100])
plt.xlabel('x')
plt.ylabel('y')
plt.title('Quadratic equation')
for x in range(-10,10):
    y=x*x
    plt.plot(x,y,'8')
plt.show()

#Line equation
a=input()
x=['',a]
y=[0,str((5*int(a))+3)]
plt.plot(x,y,'8')
plt.axis([0,10,0,30])
plt.xlabel('x')
plt.ylabel('y')
plt.title('Linear equation')
plt.show()

#Vegtable scatter plot
x=['','carrot','potato','pumpkin','broccoli']
y=[0,10,12,5,7]
plt.plot(x,y,'8')
plt.axis([0,5,0,15])
plt.xlabel('vegetables')
plt.ylabel('price')
plt.title('Prices of vegetables')
plt.show()

#Test
x=[1,2,3,4,5,6]
y=[10,2,7,4,18,9]
plt.plot(x,y,'8')
plt.axis([0,6,0,20])
plt.xlabel('x')
plt.ylabel('y')
plt.title('X vs Y')
plt.show()
'''

