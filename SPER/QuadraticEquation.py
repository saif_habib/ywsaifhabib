import math
import matplotlib
import matplotlib.pyplot as plt
import random
import csv
print("Enter a in your formula")
a = input()
a = float(a)
print("Enter b in your formula")
b = input()
b = float(b)
print("Enter c in your formula")
c = input()
c = float(c)


if (b*b-(4*a*c))>=0:
    if a!=0:
        x1 = (((-b)+(math.sqrt((b*b)-(4*a*c))))/(2*a))
        x2 = (((-b)-(math.sqrt((b*b)-(4*a*c))))/(2*a))
        aos = (-b)/(2*a)
        trm1 = a*aos*aos
        trm2 = b*aos
        yinr = (2*aos)
        vertex = trm1+trm2+c
        print("FORMULA: ",str(a)+"x^2 + "+str(b)+"x + "+str(c))
        print("FIRST X INTERCEPT:",x1)
        print("SECOND X INTERCEPT:",x2)
        print("AXIS OF SYMMETRY:",aos)
        print("VERTEX: (",aos,",", (trm1+trm2+c),")")
        print("Y INTERCEPT REFLECTION: (",yinr,", ",float(c),")")
        x1=int(x1)
        x2=int(x2)
        vertex=int(vertex)
        yinr=int(yinr)
        aos=int(aos)
    else:
        xin=((-c)/b)
        print("FORMULA: ",str(b)+"x + "+str(c))
        print("X INTERCEPT: ("+str(xin),", 0)")
    yin = c
    print("Y INTERCEPT: (0.0, ",float(c),")")

    yin=int(yin)
    if a!=0:
        if vertex<yin:
            plt.axis([(x2-10),(x1+10),vertex,(yin+10)])
        elif vertex>yin:
            plt.axis([(x2-10),(x1+10),vertex,(yin-10)])
        plt.title('Quadratic Equation')
        plt.xlabel('x')
        plt.ylabel('y')
        for x in range((int(round(x2))-10),(int(round(x1))+10),1):
            y=((a*x*x)+(b*x)+c)
            plt.plot(int(x),int(y),'.')
        plt.plot(x1,0,'8')
        plt.plot(x2,0,'8')
        plt.plot(aos,vertex,'8')
        plt.plot(yinr,c,'8')
    else:
        plt.axis([(xin-10),(xin+10),(yin-10),(yin+10)])
        plt.title('Linear Equation')
        plt.xlabel('x')
        plt.ylabel('y')
        for x in range((int(xin)-10),(int(xin)+10),1):
            y=((b*x)+c)
            plt.plot(int(x),int(y),'.')
        plt.plot(int(xin),0,'8')
    plt.plot(0,c,'8')
    plt.show()
else:
    print("No real solutions")
