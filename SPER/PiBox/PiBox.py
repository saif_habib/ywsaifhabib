def LOBBY():
    print("Welcome to the PiBox!")
    print("By: Saif Habib")
    print()
    print("The games in this include:")
    print("Coin Toss")
    print("Lottory")
    print("Hangman")
    print()
    print("Type in the game as you see above to access the game.")
    print()
    x = input()
    if x == "Coin Toss":
        import COIN_TOSS
        COINTOSS.TOSS()
        LOBBY()
    elif x == "Lottory":
        import LOTTORY
        LOTTORY.RANDOM()
        LOBBY()
    elif x == "Hangman":
        print("Password is 'ORDID I'")
        import HANGMAN
        HANGMAN.HANGMAN()
        LOBBY()
    else:
        LOBBY()     
LOBBY()
