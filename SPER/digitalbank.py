                        #Digital_Bank@Ras.Pi

            #Functions

        #Quarters
def Q():
    print("Enter the amount of quarters you have.")
    q = input()
    Q = int(q)
    a = Q * 0.25
    print(round(a,2), "$ is the amout of money in quarters you have.")
    return a
        #Dimes
def D():
    print("Enter the amount of dimes you have.")
    d = input()
    D = int(d)
    b = D * 0.10
    print(round(b,2), "$ is the amout of money in dimes you have.")
    return b
        #Nickels
def N():
    print("Enter the amount of nickels you have.")
    n = input()
    N = int(n)
    c = N * 0.05
    print(round(c,2), "$ is the amout of money in nickels you have.")
    return c
        #Pennies
def P():
    print("Enter the amount of pennies you have.")
    p = input()
    P = int(p)
    d = P * 0.01
    print(round(d,2), "$ is the amout of money in pennies you have.")
    return d
            #Introduction
print("Welcome to Digital_Bank@Ras.Pi!")
print("Digital_Bank@Ras.Pi will tell you how much money you have.")
print("Answer all the questions.")
print("The order it will go in will be: Quarters, Dimes, Nickels, and Pennies.")
            #Run The Functions (Variables to add)
x = Q()
y = D()
z = N()
s = P()
            #Conclusion
M = x + y + z + s 
print(round(M,2), "$ is the amount of money you have.")
print("Thank you for using Digital_Bank@Ras.Pi!")
