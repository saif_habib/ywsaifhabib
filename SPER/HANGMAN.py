def HANGMAN():
    print("WHAT'S THE PASSCODE")
    passcode = input()
    if passcode == "ORDID I":
        import random
        print("Guess my word!")
        words = ["Apple", "Hello", "Games", "Tried", "Timed", "Linux"]
        print("You have 15 chances.")
        rl = 0
        d = random.randint(0,3)
        t = ["Hack", "Fix", "Word", "Hi"]
        l = t[d]
        word = random.randint(0,4)
        wordinlist = words[word]
        Word = str(wordinlist)
        wordII = list(Word)
        Hangman = ["_", "_", "_", "_", "_"]
        n = 0
        while "".join(Hangman) != word or n < 15:
            print("Enter a letter in my word. Use Cntrl + C to quit.")
            print(Hangman)
            x = input()
            if x == l:
                if rl < 1:
                    print(wordII[random.randint(0,4)])
                    rl += 1
                    n += 1
            elif x in wordII:
                print("That is correct!")
                for pos in range(len(wordII)):
                    if wordII[pos] == x:
                        Hangman[pos] = x
                        n += 1
            else:
                print("Try again!")
                n += 1
            if n == 15:
                print("Your chances are up!")
                print("Try again!")
            elif Hangman == wordII:
                break
        print(Hangman)
        print("You guessed my word! It is", Word, "!")
        print("Do you want to continue?")
        hi = input()
        if hi == "Yes":
            HANGMAN()
        else:
            print("THANK YOU FOR PLAYING HANGMAN!!!")
HANGMAN()
