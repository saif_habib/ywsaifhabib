def TOSS():
    import random
    print("HEADS OR TAILS?")
    b=["HEADS", "TAILS"]
    a = input()
    c = random.choice(b)
    if a == c:
        print(c)
        print("YOU ARE CORRECT!")
    else:
        print(c)
        print("YOU ARE WRONG!")
    print("DO YOU WANT TO TRY AGAIN?")
    print("y for Yes and n for No.")
    d = input()
    if d == "y":
        TOSS()
    else:
        print("THANK YOU FOR DOING A COIN TOSS WITH ME!")
TOSS()
