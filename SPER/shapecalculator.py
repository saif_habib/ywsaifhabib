#Shape Code
import SHAPE_CALCULATOR__SHAPES_FILE
def SHAPES():
    #Intro
    print("Welcome to SHAPE CALCULATOR!!!.")
    print("What Shape do you want me to calculate the area of?")
    print("Options: 'Rectangle', 'Triangle', or 'Circle'.")
    shape = input()
    #Run the shape functions
    if shape == "Rectangle":
        SHAPE_CALCULATOR__SHAPES_FILE.R()
    elif shape == "Triangle":
        SHAPE_CALCULATOR__SHAPES_FILE.T()
    elif shape == "Circle":
        SHAPE_CALCULATOR__SHAPES_FILE.C()
    else:
        print("Please try again.")
        SHAPES()
    #End or continue
    print("Press 'x' to end.")
    print("Press '_' to continue.")
    d = input()
    if d == "x":
        print("Thank you for using SHAPE CALCULATOR!")
    elif d == "_":
        SHAPES()
    else:
        SHAPES()
SHAPES()
