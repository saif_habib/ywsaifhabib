string = "Strings Are Cool!!!"
print(string)
print(string.capitalize())
print(string.title())
print(string.lower())
print(string.upper())
print(string.replace("a","c"))
print(string.find("o"))
print()
name = "Saif"
age = "11 years"
state = "California"
stringvar = "My name is {0}. I am {1} old. I live in {2}.".format(name, age, state)
print(stringvar.endswith("ing"))
print(stringvar.isalpha())
print(stringvar.isnumeric())
print(stringvar.isupper())
print(stringvar.islower())
print(stringvar)
print()
dateStr = "9/4/2016"
mdy = dateStr.split("/")
print(mdy)
print()
print("FACE!")
print("   ~~~~~~~   ")
print(" ~~~~~~~~~~~ ")
print("-------------")
print("|[()] | [()]|")
print("|    < >    |")
print("|   \___/   |")
print(" ___________ ")
print("   -------   ")
print()
stringhi = "   Hello Guys!!!   "
print(stringhi)
print(stringhi.strip())
print()
