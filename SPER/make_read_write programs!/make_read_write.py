def file():
    print("What is the name of the source file you want to create?")
    fileI = input()
    sfile = fileI + '.txt'
    source = open(sfile, 'w')
    print(sfile)
    print("What is the name of the destination file you want to create?")
    fileII = input()
    dfile = fileII + '.txt'
    desfile = open(dfile, 'w')
    print(dfile)
    print("Hello World", file = source)
    print("Hello Python", file = source)
    print("Hello RasPi", file = source)
    print("Hello File", file = source)
    print("Hello Again", file = source)
    source.close()
    rsource = open(sfile, 'r')
    for lines in rsource:
        first,second = lines.split()
        uname = (first[0]+second).lower()
        print(uname, file = desfile)
    print("Check in", sfile, "and", dfile, "for your names and usernames.")
    rsource.close()
    desfile.close()
file()
