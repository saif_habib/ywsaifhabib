import pygame
import time
from pygame.locals import *
import socket
pygame.init()
WIDTH=500
HEIGHT=500
direction=0
window=pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption("Simple Multiplayer [CLIENT]")
host="192.168.1.75"
port=1234
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))
class Player1:
    def draw(self):
        xlen = s.recv(1).decode("utf-8")
        xx = s.recv(int(xlen)).decode("utf-8")
        ylen = s.recv(1).decode("utf-8")
        yy = s.recv(int(ylen)).decode("utf-8")
        color = (0,102,204)
        pygame.draw.rect(window,color,(int(xx),int(yy),20,20))
class Player2:
    def __init__(self):
        self.x=WIDTH//2
        self.y=HEIGHT//2
    def moveBy(self,direction):
        if direction==1 and self.y!=0:
            self.y-=20
        if direction==2 and self.y!=(HEIGHT-20):
            self.y+=20
        if direction==3 and self.x!=0:
            self.x-=20
        if direction==4 and self.x!=(WIDTH-20):
            self.x+=20
        if direction==5:
            self.x=self.x
            self.y=self.y
    def draw(self):
        pygame.draw.rect(window,(0,204,102),(self.x,self.y,20,20))
player1=Player1()
player2=Player2()
while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            exit()
        if event.type==KEYDOWN:
            if event.key==K_UP:
                direction=1
            elif event.key==K_DOWN:
                direction=2
            elif event.key==K_LEFT:
                direction=3
            elif event.key==K_RIGHT:
                direction=4
            elif event.key==K_SPACE:
                direction=0
    time.sleep(0.01)
    player2.moveBy(direction)
    window.fill((0,0,0))
    s.send(bytes("{}".format(len(str(player2.x))),"utf-8"))
    s.send(bytes("{}".format(player2.x),"utf-8"))
    s.send(bytes("{}".format(len(str(player2.y))),"utf-8"))
    s.send(bytes("{}".format(player2.y),"utf-8"))
    player2.draw()
    player1.draw()
    pygame.display.update()
s.close()

##-Xmx1800M -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn512M
