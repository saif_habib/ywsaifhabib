#CLIENT
#Receive pipe x length
#Receive pipe x
#Receive player 1 y length
#Receive player 1 y
#Send player 2 y length
#Send player 2 y

##Open server
import pygame
import time
import random
import socket
from pygame.locals import *
pygame.init()
window=pygame.display.set_mode((500,500))
pygame.display.set_caption("Flappy Block [Client]")
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host="192.168.1.75"
port=1234
s.connect((host,port))

class Player1:
    def draw(self):
        self.ylen=s.recv(1).decode("utf-8")
        self.y=s.recv(int(self.ylen)).decode("utf-8")
        pygame.draw.rect(window,(0,102,204),(125,int(self.y),25,25))

class Player2:
    def __init__(self):
        self.x=125
        self.y=125
        self.key=""
    def move(self):
        pygame.draw.rect(window,(0,204,102),(int(self.x),int(self.y),25,25))
        d=list(str(pygame.event.poll()))[12]
        if d=="U":      
            self.key="up"
        elif d=="D":
            self.key="down"
        if self.key=="down":
            self.y-=10
        elif self.key=="up":
            self.y+=8
        pygame.event.clear()
        s.send(bytes("{}".format(int(len(str(self.y)))),"utf-8"))
        s.send(bytes("{}".format(self.y),"utf-8"))

    def check(self):
        if int(self.x)>=int(pipex) and (int(self.x)+25)<=(int(pipex)+80) and (int(self.y)<=int(holey) or (int(self.y)+25)>=(int(holey)+100) or int(self.y)>=480 or int(self.y)<=0):
            s.sendall(bytes("{}".format(888),"utf-8"))
        else:
            s.sendall(bytes("{}".format(999),"utf-8"))

        gamestat=s.recv(1).decode("utf-8")
        if gamestat=="1":
            print("Player 1 Wins!")
            exit()
        elif gamestat=="2":
            print("Player 2 Wins!")
            exit()
        elif gamestat=="3":
            print("Tie!")
            exit()

class Pipe:
    def draw(self):
        self.xlen=s.recv(1).decode("utf-8")
        self.x=s.recv(int(self.xlen)).decode("utf-8")
        pygame.draw.rect(window,(255,255,255),(int(self.x),0,80,500))
        self.holelen=s.recv(1).decode("utf-8")
        self.hole=s.recv(int(self.holelen)).decode("utf-8")
        pygame.draw.rect(window,(0,0,0),(int(self.x),int(self.hole),80,100))
        global holey, pipex
        holey=self.hole
        holey=int(holey)
        pipex=self.x
        pipex=int(pipex)

pipe=Pipe()
player1=Player1()
player2=Player2()
while True:
    pipe.draw()
    player1.draw()
    player2.move()
    player2.check()
    pygame.display.update()
    time.sleep(0.01)
    window.fill((0,0,0))
s.close()
