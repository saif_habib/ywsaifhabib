#SERVER
#Send pipe x length
#Send pipe x
#Send player 1 y length
#Send player 1 y
#Receive player 2 y length
#Receive player 2 y

##Reset Server Address
#ps
#kill -9 <bash#>
import pygame
import time
import random
import socket
from pygame.locals import *
pygame.init()
window=pygame.display.set_mode((500,500))
pygame.display.set_caption("Flappy Block [Server]")
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(("192.168.1.75",1234))
s.listen(10)
client, client_address=s.accept()
print("Client has connected with address: {}".format(client_address))

class Player1:
    def __init__(self):
        self.x=125
        self.y=125
        self.key=""
    def move(self):
        pygame.draw.rect(window,(0,204,102),(int(self.x),int(self.y),25,25))
        d=list(str(pygame.event.poll()))[12]
        if d=="U":      
            self.key="up"
        elif d=="D":
            self.key="down"
        if self.key=="down":
            self.y-=10
        elif self.key=="up":
            self.y+=8
        pygame.event.clear()
        client.sendall(bytes("{}".format(int(len(str(self.y)))),"utf-8"))
        client.sendall(bytes("{}".format(self.y),"utf-8"))

    def check(self):
        #999 is pass
        #888 is fail
        p2stat=client.recv(3).decode("utf-8")
        p2stat=int(p2stat)
        if int(self.x)>=int(pipex) and (int(self.x)+25)<=(int(pipex)+80) and (int(self.y)<=int(holey) or (int(self.y)+25)>=(int(holey)+100) or int(self.y)>=480 or int(self.y)<=0):
            p1stat=888
        else:
            p1stat=999

        if p1stat==888 and p2stat==888:
            client.sendall(bytes("{}".format(3),"utf-8"))
            print("Tie!")
            exit()
        elif p2stat==888 and p1stat==999:
            client.sendall(bytes("{}".format(1),"utf-8"))
            print("Player 1 Wins!")
            exit()
        elif p1stat==888 and p2stat==999:
            client.sendall(bytes("{}".format(2),"utf-8"))
            print("Player 2 Wins!")
            exit()
        else:
            client.sendall(bytes("{}".format(0),"utf-8"))

class Player2:
    def draw(self):
        self.ylen=client.recv(1).decode("utf-8")
        self.y=client.recv(int(self.ylen)).decode("utf-8")
        pygame.draw.rect(window,(0,102,204),(125,int(self.y),25,25))

class Pipe:
    def __init__(self):
        self.x=500
        self.y=0
        self.hole=random.randint(20,400)
    def move(self):
        if not self.x<0:
            self.x-=5
        else:
            self.x=500
            self.y=0
            self.hole=random.randint(20,380)
        client.sendall(bytes("{}".format(int(len(str(self.x)))),"utf-8"))
        client.sendall(bytes("{}".format(self.x),"utf-8"))
        pygame.draw.rect(window,(255,255,255),(int(self.x),int(self.y),80,500))
        client.sendall(bytes("{}".format(int(len(str(self.hole)))),"utf-8"))
        client.sendall(bytes("{}".format(self.hole),"utf-8"))
        pygame.draw.rect(window,(0,0,0),(int(self.x),int(self.hole),80,100))
        global holey, pipex
        holey=self.hole
        holey=int(holey)
        pipex=self.x
        pipex=int(pipex)

pipe=Pipe()
player1=Player1()
player2=Player2()
while True:
    time.sleep(0.01)
    pipe.move()
    player1.move()
    player2.draw()
    player1.check()
    pygame.display.update()
    window.fill((0,0,0))

