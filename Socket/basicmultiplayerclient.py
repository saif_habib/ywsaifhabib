import pygame
import time
from pygame.locals import *
import socket
pygame.init()
WIDTH=500
HEIGHT=500
direction=0
window=pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption("Simple Multiplayer [CLIENT]")
host="192.168.1.249"
port=1234
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))
class Player1:
    def draw(self):
        xlen = s.recv(1).decode("utf-8")
        xx = s.recv(int(xlen)).decode("utf-8")
        ylen = s.recv(1).decode("utf-8")
        yy = s.recv(int(ylen)).decode("utf-8")
        color = (0,102,204)
        pygame.draw.rect(window,color,(int(xx),int(yy),20,20))
player1=Player1()
while True:
    time.sleep(0.01)
    window.fill((0,0,0))
    player1.draw()
    pygame.display.update()
s.close()
