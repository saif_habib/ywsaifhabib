import socket
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(("0.0.0.0",1234))
s.listen(10)
send=b"""GET HTTP/1.1 200 OK\r\n
<!DOCTYPE html>
<html>
 <head>
  <title>
   Open Server
  </title>
  <h1 style="color:blue;">
   Hello
  </h1>
 </head>
 <body style="background-color:lightblue;">
  <p style="color:green;">A Python web server is serving this webpage.</p>
 </body>
</html>"""
while True:
    browser,browser_address=s.accept()
    print("Browser connected at address: {}".format(browser_address))
    ch=None
    message=""
    while message[len(message)-2:]!='\r\n':
        ch=browser.recv(1).decode('utf-8')
        message=message+ch
    browser.sendall(send)
    print("Message from browser: "+message)
browser.close()
